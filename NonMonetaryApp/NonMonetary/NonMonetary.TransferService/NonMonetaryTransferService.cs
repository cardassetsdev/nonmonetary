﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace NonMonetary.TransferService
{
    public partial class NonMonetaryTransferService : ServiceBase
    {
        Timer _executionTimer = new Timer(60000);

        public NonMonetaryTransferService()
        {
            InitializeComponent();

            _executionTimer.Elapsed += new ElapsedEventHandler(_executionTimer_Elapsed);
        }

        void _executionTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

        }

        protected override void OnStart(string[] args)
        {
            _executionTimer.Enabled = true;
        }

        protected override void OnStop()
        {
            _executionTimer.Enabled = false;
        }
    }
}
