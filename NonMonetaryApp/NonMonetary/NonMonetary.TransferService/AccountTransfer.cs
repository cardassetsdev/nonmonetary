﻿using NonMonetary.Interop;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP;

namespace NonMonetary.TransferService
{
    public class AccountTransfer: INonMonProcessor
    {
        int _index = 0;
        List<INonMonRecord> _records;
        
        /// <summary>
        /// NonMonProcessing Status
        /// </summary>
        private NonMonProcessingStatus _status = NonMonProcessingStatus.Pending;
        public NonMonProcessingStatus Status
        {
            get { return _status; }
        }

        public event EventHandler<NonMonRecordArgs> OnRecordCompleted;

        public event EventHandler OnStatusChanged;

        public event EventHandler<System.ComponentModel.ProgressChangedEventArgs> OnProgressChanged;

        public void Configure(System.Xml.XPath.IXPathNavigable xPathNavigable)
        {
        }

        /// <summary>
        /// Name of processor
        /// </summary>
        private string _processorName;
        public string ProcessorName
        {
            get {return _processorName;}
            set {_processorName = value;}
        }

       
        public void ProcessNonMonRecords(List<INonMonRecord> records)
        {
            try
            {
                _index = 0;

                _records = records;

                Process();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        public void Abort()
        {
        }

        public void Pause()
        {            
        }

        public void Continue()
        {
        }

        private static void WriteLogEntry(
           LogEntryArgs logArgs
           )
        {
            Type declaringType = logArgs.MemberInfo.DeclaringType;

            log4net.ILog logger = log4net.LogManager.GetLogger(
                declaringType != null
                ? declaringType
                : logArgs.MemberInfo as Type
                );

            switch (logArgs.LogLevel)
            {
                case LogLevel.Fatal:
                    if (logger.IsFatalEnabled)
                        logger.Fatal(logArgs.ReadMessage());
                    break;
                case LogLevel.Error:
                    if (logger.IsErrorEnabled)
                        logger.Error(logArgs.ReadMessage());
                    break;
                case LogLevel.Warn:
                    if (logger.IsWarnEnabled)
                        logger.Warn(logArgs.ReadMessage());
                    break;
                case LogLevel.Info:
                    if (logger.IsInfoEnabled)
                        logger.Info(logArgs.ReadMessage());
                    break;
                case LogLevel.Debug:
                    if (logger.IsDebugEnabled)
                        logger.Debug(logArgs.ReadMessage());
                    break;
                default:
                    logger.Warn(LogMessages.FollowingEntryHasInvalidLevel);
                    logger.Info(logArgs.ReadMessage());
                    break;
            }
        }
        
        #region private Methods
        /// <summary>
        /// Row by row processing of data set
        /// </summary>
        private void Process()
        {
            try
            {
                _status = NonMonProcessingStatus.InProcess;

                if (OnStatusChanged != null)
                    OnStatusChanged(this, new EventArgs());

                int max = _records.Count;

                string host = ConfigurationManager.AppSettings.Get(Resources.Fields.CleoHost);
                int port = Convert.ToInt32(ConfigurationManager.AppSettings.Get(Resources.Fields.CleoPort));

                TPConnector connector = new TPConnector(host, port);
                try
                {
                    if (connector.Reserve(System.Configuration.ConfigurationManager.AppSettings.Get(Resources.Fields.TransactionSetName)) == 0)
                    {
                        INonMonRecord record;

                        while (_index < max)
                        {
                            record = _records[_index];

                            //if ((_status == NonMonProcessingStatus.Aborted) ||
                            //    (_status == NonMonProcessingStatus.Paused))
                            //    break;

                            //if (this.OnProgressChanged != null)
                            //    OnProgressChanged(
                            //        this,
                            //        new ProgressChangedEventArgs(
                            //            (_index * 100) / max,
                            //            String.Format("Processing {0} of {1}", _index + 1, max)));

                            PerformNonMon(record, connector);

                            if (this.OnRecordCompleted != null)
                                this.OnRecordCompleted(this, new NonMonRecordArgs(record));

                            _index++;
                        }

                        if (_index == max)
                        {
                            this._status = NonMonProcessingStatus.Completed;

                            if (OnStatusChanged != null)
                                OnStatusChanged(this, new EventArgs());

                            //if (OnProgressChanged != null)
                            //    OnProgressChanged(
                            //        this,
                            //        new ProgressChangedEventArgs(
                            //            100,
                            //            String.Format(
                            //                "{0} records processed",
                            //                max)));
                        }
                    }
                }
                finally
                {
                    connector.Release();
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private void PerformNonMon(INonMonRecord record, TPConnector connector)
        {
            try
            {
                if (!record.FieldValueExists(Resources.Fields.AccountNumber) ||
                    !record.FieldValueExists(Resources.Fields.LastName) ||
                    !record.FieldValueExists(Resources.Fields.NewSysPrinAgent))
                    throw new Exception(LogMessages.RecordMissingFields);

                record.Description = String.Format(
                    "Account transfer for account {0} to SysPrinAgent {1}.",
                    TrimString(record.GetFieldValue(Resources.Fields.AccountNumber)),
                    TrimString(record.GetFieldValue(Resources.Fields.NewSysPrinAgent)));

                connector.ResetInput();

                connector.AddInput(Resources.Fields.AccountNumber, TrimString(record.GetFieldValue(Resources.Fields.AccountNumber)));
                connector.AddInput(Resources.Fields.TransferType, "N");
                connector.AddInput(Resources.Fields.LastName, TrimString(record.GetFieldValue(Resources.Fields.LastName)));
                connector.AddInput(Resources.Fields.LetterNumber, TrimString(record.GetFieldValue(Resources.Fields.LetterNumber)));
                connector.AddInput(Resources.Fields.ProducePlastics, "Y");

                if (String.IsNullOrEmpty(record.GetFieldValue(Resources.Fields.NewAccountNumber)))
                {
                    connector.AddInput(Resources.Fields.NewSysPrinAgent, TrimString(record.GetFieldValue(Resources.Fields.NewSysPrinAgent)));
                    connector.AddInput(Resources.Fields.AutoAccountAssign, "Y");
                    connector.AddInput(Resources.Fields.NewAccountNumber, String.Empty);
                }
                else
                {
                    connector.AddInput(Resources.Fields.NewSysPrinAgent, String.Empty);
                    connector.AddInput(Resources.Fields.AutoAccountAssign, String.Empty);
                    connector.AddInput(Resources.Fields.NewAccountNumber, TrimString(record.GetFieldValue(Resources.Fields.NewAccountNumber)));
                }

                if (String.IsNullOrEmpty(TrimString(record.GetFieldValue(Resources.Fields.RushIndicator))))
                    connector.AddInput(Resources.Fields.RushIndicator, String.Empty);
                else
                    connector.AddInput(Resources.Fields.RushIndicator, TrimString(record.GetFieldValue(Resources.Fields.RushIndicator)));

                record.ReturnCode = connector.RunTransaction(Resources.Fields.AccountTransfer);

                if (record.ReturnCode == 0)
                    record.ReturnMessage = TrimString(connector.GetOutput(Resources.Fields.ReturnMessage));
                else
                    record.ReturnMessage = "Transaction Failed";

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }
        private string TrimString(string inputString)
        {
            return (inputString == null) ? String.Empty : inputString.Trim();
        }

#endregion
    }
}
