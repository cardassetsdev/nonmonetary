﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;

namespace NonMonetary.Interface
{
    public interface INonMonViewPresenter
    {
        List<INonMonRecord> Records { get; }
        INonMonView View { get; set; }
        void Process();
        void Abort();
        void LoadNonMonProcessors();
    }
}
