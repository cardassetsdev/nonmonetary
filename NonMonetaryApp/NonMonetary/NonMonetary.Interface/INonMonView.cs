﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using NonMonetary.Interop;

namespace NonMonetary.Interface
{
    public interface INonMonView
    {
        string AbortContent { get; set; }
        bool CanAbort { get; set; }
        bool CanExecute { get; set; }
        bool CanSelectFile { get; set; }
        bool CanSelectProcessor { get; set; }
        string ExecuteContent { get; set; }
        string FileName { get; set; }
        INonMonViewPresenter Presenter { get; set; }
        INonMonProcessor SelectedProcessor { get; }
        EventHandler OnFileSelected { get; set; }
        EventHandler OnProcessorChanged { get; set; }
        void LoadNonMonList(List<INonMonProcessor> list);
        void SetProgress(object sender, ProgressChangedEventArgs e);
    }
}
