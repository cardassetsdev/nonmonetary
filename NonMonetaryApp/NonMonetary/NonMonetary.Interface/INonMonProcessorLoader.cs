﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;

namespace NonMonetary.Interface
{
    public interface INonMonProcessorLoader
    {
        List<INonMonProcessor> LoadProcessors(string filePath);
    }
}
