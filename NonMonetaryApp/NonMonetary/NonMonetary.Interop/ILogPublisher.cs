﻿using System;

namespace NonMonetary.Interop
{
    public interface ILogPublisher
    {
        void RegisterLogEntryCallback(LogEntryCallback callback);
    }
}