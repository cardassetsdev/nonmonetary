﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NonMonetary.Interop
{
    public interface INonMonPersistence
    {
        void Initialize(INonMonProcessor processor);
        void AddRecord(INonMonRecord record);
        void Persist();
    }
}
