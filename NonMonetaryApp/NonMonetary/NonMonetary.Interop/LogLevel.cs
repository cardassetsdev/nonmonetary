﻿using System;

namespace NonMonetary.Interop
{
    public enum LogLevel
    {
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
    }
}
