﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NonMonetary.Interop
{
    [Serializable]
    public sealed class NonMonRecordArgs : EventArgs
    {
        private INonMonRecord _record;

        public NonMonRecordArgs(
            INonMonRecord record
            )
            : base()
        {
            this._record = record;
        }

        public INonMonRecord Record { get { return this._record; } }
    }
}
