﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NonMonetary.Interop
{
    public interface INonMonRecordLoader
    {
        List<INonMonRecord> LoadRecords(string fileName);
    }
}
