﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NonMonetary.Interop
{
    public enum NonMonProcessingStatus
    {
        Aborted,
        Completed,
        InProcess,
        Paused,
        Pending
    }
}
