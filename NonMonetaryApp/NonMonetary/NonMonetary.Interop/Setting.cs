﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NonMonetary.Interop
{
    [
    Serializable(),
    XmlRoot(
        ElementName = "Setting",
        Namespace = "http://xml.cs.silvertonbank.net/nonmonetary/configuration/",
        IsNullable = true
        ),
    XmlType(
        TypeName = "Setting",
        Namespace = "http://xml.cs.silvertonbank.net/nonmonetary/configuration/"
        )
    ]
    public sealed class Setting
    {

        private String _name;
        private String _value;

        public Setting() { }

        public Setting(
            String name,
            String value
            )
        {
            this._name = name;
            this._value = value;
        }

        [XmlAttribute(AttributeName = "name", DataType = "Name")]
        public String Name
        {
            get { return this._name; }
            set
            {
                if (this._name != value)
                    this._name = value;
            }
        }

        [XmlAttribute(AttributeName = "value")]
        public String Value
        {
            get { return this._value; }
            set
            {
                if (this._value != value)
                    this._value = value;
            }
        }
    }
}
