﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NonMonetary.Interop
{
    [Serializable]
    public sealed class SettingCollection
        : System.Collections.ObjectModel.Collection<Setting>
    {
        private Dictionary<String, Setting> _settings = new Dictionary<String, Setting>();

        public String this[String key]
        {
            get
            {
                if (this._settings.ContainsKey(key))
                    return this._settings[key].Value;
                else
                    return null;
            }

            set
            {
                if (key == null)
                    throw new ArgumentNullException("key");
                if (value == null)
                    throw new ArgumentNullException("value");

                if (this._settings.ContainsKey(key))
                    this.Remove(this._settings[key]);

                this._settings[key] = new Setting(key, value);
                this.Add(this._settings[key]);
            }
        }

        public bool Contains(string key)
        {
            return this._settings.ContainsKey(key);
        }

        protected override void InsertItem(Int32 index, Setting item)
        {
            base.InsertItem(index, item);

            if (item != null)
                this._settings[item.Name] = item;
        }

        protected override void SetItem(Int32 index, Setting item)
        {
            if (index < this.Count)
            {
                Setting setting = this[index];
                if (setting != null)
                    this._settings.Remove(setting.Name);
            }

            base.SetItem(index, item);

            if (item != null)
                this._settings[item.Name] = item;
        }

        protected override void RemoveItem(Int32 index)
        {
            Setting setting = this[index];
            base.RemoveItem(index);

            if (setting != null)
                this._settings.Remove(setting.Name);
        }

        protected override void ClearItems()
        {
            base.ClearItems();
            this._settings.Clear();
        }
    }
}

