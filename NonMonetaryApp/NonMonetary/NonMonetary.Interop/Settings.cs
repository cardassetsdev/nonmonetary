﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace NonMonetary.Interop
{
    [
    Serializable(),
    XmlRoot(
        ElementName = "Settings",
        Namespace = "http://xml.cs.silvertonbank.net/nonmonetary/configuration/",
        IsNullable = false
        ),
    XmlType(
        TypeName = "Settings",
        Namespace = "http://xml.cs.silvertonbank.net/nonmonetary/configuration/"
        )
    ]
    public sealed class Settings
    {
        private SettingCollection _settingCollection = new SettingCollection();

        public Settings() { }

        public Settings(
            SettingCollection settings
            )
        {
            this._settingCollection = settings;
        }

        public String this[String key]
        {
            get { return this._settingCollection[key]; }
            set { this._settingCollection[key] = value; }
        }

        [XmlElement("Setting")]
        public SettingCollection Setting
        {
            get { return this._settingCollection; }
        }

        public static Settings LoadSettings(
            IXPathNavigable settings
            )
        {
            if (settings == null)
                return null;

            XPathNavigator settingsNav = settings.CreateNavigator();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            return xmlSerializer.Deserialize(settingsNav.ReadSubtree()) as Settings;
        }
    }
}

