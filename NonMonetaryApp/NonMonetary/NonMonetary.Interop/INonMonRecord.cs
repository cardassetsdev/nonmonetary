﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NonMonetary.Interop
{
    public interface INonMonRecord
    {
        void AttachToRecord(DataRow row);
        String ReturnMessage { get; set; }
        int ReturnCode { get; set; }
        String GetFieldValue(String fieldName);
        bool FieldValueExists(String fieldName);
        String Description { get; set; }
    }
}
