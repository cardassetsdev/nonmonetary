﻿using System;
using System.Reflection;
using System.Globalization;

namespace NonMonetary.Interop
{
    [Serializable]
    public sealed class LogEntryArgs
    {
        private LogLevel _level;
        private MemberInfo _memberInfo;
        private String _message;

        private IFormatProvider _provider;
        private String _format;
        private Object[] _args;

        private LogEntryArgs(
            LogLevel level,
            MemberInfo memberInfo
            )
            : base()
        {
            if (memberInfo == null)
                throw new ArgumentNullException("memberInfo");

            this._level = level;
            this._memberInfo = memberInfo;
        }

        public LogEntryArgs(
            LogLevel level,
            MemberInfo memberInfo,
            String message
            )
            : this(level, memberInfo)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            this._message = message;
        }

        public LogEntryArgs(
            LogLevel level,
            MemberInfo memberInfo,
            IFormatProvider provider,
            String format,
            params Object[] args
            )
            : this(level, memberInfo)
        {
            if (format == null)
                throw new ArgumentNullException("format");
            if (args == null)
                throw new ArgumentNullException("args");

            this._provider = provider;
            this._format = format;
            this._args = args;
        }

        public LogLevel LogLevel { get { return this._level; } }
        public MemberInfo MemberInfo { get { return this._memberInfo; } }

        public String Message { get { return this._message; } }

        public IFormatProvider Provider { get { return this._provider; } }
        public String Format { get { return this._format; } }

        public Object[] GetArgs()
        {
            return this._args;
        }

        public String ReadMessage()
        {
            if (this.Message != null)
                return Message;

            return String.Format(this.Provider, this.Format, this.GetArgs());
        }
    }
}

