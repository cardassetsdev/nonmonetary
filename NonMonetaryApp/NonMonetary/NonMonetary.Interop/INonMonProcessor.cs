﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml.XPath;

namespace NonMonetary.Interop
{
    public interface INonMonProcessor
    {
        NonMonProcessingStatus Status { get; }
        event EventHandler<NonMonRecordArgs> OnRecordCompleted;
        event EventHandler OnStatusChanged;
        event EventHandler<ProgressChangedEventArgs> OnProgressChanged;
        string ProcessorName { get; set; }
        void Configure(IXPathNavigable xPathNavigable);
        void ProcessNonMonRecords(List<INonMonRecord> records);
        void Abort();
        void Pause();
        void Continue();
    }
}
