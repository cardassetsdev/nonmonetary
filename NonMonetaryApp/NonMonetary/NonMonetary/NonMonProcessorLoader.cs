﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using NonMonetary.Interop;
using NonMonetary.Interface;
using System.Diagnostics;
using System.Xml.XPath;
using NonMonetary.Resources;
using System.Windows.Forms;

namespace NonMonetary
{
    public class NonMonProcessorLoader : INonMonProcessorLoader, ILogPublisher
    {
        public NonMonProcessorLoader()
        {
            ConfigurationManager.AttachLogEntryCallback(this);
        }

        private static bool InterfaceFilter(Type typeObj, Object criteriaObj)
        {
            if (typeObj.ToString() == criteriaObj.ToString())
                return true;
            else
                return false;
        }

        #region INonMonProcessorLoader
        
        /// <summary>
        /// Search the startup directory for assemblies that contain INonMonProcessors
        /// </summary>
        public List<INonMonProcessor> LoadProcessors(string filePath)
        {
            List<INonMonProcessor> list = new List<INonMonProcessor>();
            try
            {
                //find all dlls in the start up directory
                XPathDocument config = ConfigurationManager.LoadConfigurationDocument(Path.Combine(filePath, System.Configuration.ConfigurationManager.AppSettings[Attributes.NonMonetaryConfigSource].ToString()));
                
                IEnumerable<IXPathNavigable> navs = ConfigurationManager.SelectNodes(XPath.SelectConfiguredProcessors, config.CreateNavigator());

                String assemblyPath;
                String typeName;
                Assembly assembly;
                Type type;

                foreach (IXPathNavigable nav in navs)
                {
                    typeName = ConfigurationManager.GetAttributeValue(nav, Attributes.Type);
                    assemblyPath = Path.Combine(Path.GetDirectoryName(Assembly.GetCallingAssembly().Location), ConfigurationManager.GetAttributeValue(nav, Attributes.Path));
                    assembly = Assembly.LoadFile(assemblyPath);
                    type = assembly.GetType(typeName);

                    if (type != null)
                    {
                        NonMonetary.Interop.INonMonProcessor processor = (NonMonetary.Interop.INonMonProcessor)Activator.CreateInstance(type);
                        processor.ProcessorName = ConfigurationManager.GetAttributeValue(nav, Attributes.DisplayName);
                        processor.Configure(nav);
                        list.Add(processor);
                    }
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                this.WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }

            return list;
        }
        
        #endregion

        #region ILogPublisher Members

        private LogEntryCallback _logEntryCallback;
        protected virtual void WriteLogEntry(LogLevel level, MemberInfo info, string message)
        {
            LogEntryArgs entry = new LogEntryArgs(
                level,
                info,
                message);

            this._logEntryCallback(entry);
        }

        public void RegisterLogEntryCallback(LogEntryCallback callback)
        {
            this._logEntryCallback = callback;
        }

        #endregion
    }
}
