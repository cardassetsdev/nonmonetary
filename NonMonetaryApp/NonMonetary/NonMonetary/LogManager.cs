﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using NonMonetary.Interop;
using NonMonetary.Resources;

namespace NonMonetary
{
    internal static class LogManager
    {
        public static void WriteLogEntry(
            LogLevel level,
            String message
            )
        {
            StackTrace stackTrace = new StackTrace(1);
            if (stackTrace.FrameCount > 0)
                WriteLogEntry(
                    level,
                    stackTrace.GetFrame(0).GetMethod(),
                    message
                    );
        }

        public static void WriteLogEntry(
            LogLevel level,
            IFormatProvider provider,
            String format,
            params Object[] args
            )
        {
            StackTrace stackTrace = new StackTrace(1);
            if (stackTrace.FrameCount > 0)
                WriteLogEntry(
                    level,
                    stackTrace.GetFrame(0).GetMethod(),
                    provider,
                    format,
                    args
                    );
        }

        public static void WriteLogEntry(
            LogLevel level,
            MemberInfo memberInfo,
            String message
            )
        {
            LogEntryArgs logArgs = new LogEntryArgs(
                level,
                memberInfo,
                message
                );
            WriteLogEntry(logArgs);
        }

        public static void WriteLogEntry(
            LogLevel level,
            MemberInfo memberInfo,
            IFormatProvider provider,
            String format,
            params Object[] args
            )
        {
            LogEntryArgs logArgs = new LogEntryArgs(
                level,
                memberInfo,
                provider,
                format,
                args
                );
            WriteLogEntry(logArgs);
        }

        public static void WriteLogEntry(
            LogEntryArgs logArgs
            )
        {
            Type declaringType = logArgs.MemberInfo.DeclaringType;

            log4net.ILog logger = log4net.LogManager.GetLogger(
                declaringType != null
                ? declaringType
                : logArgs.MemberInfo as Type
                );

            switch (logArgs.LogLevel)
            {
                case LogLevel.Fatal:
                    if (logger.IsFatalEnabled)
                        logger.Fatal(logArgs.ReadMessage());
                    break;
                case LogLevel.Error:
                    if (logger.IsErrorEnabled)
                        logger.Error(logArgs.ReadMessage());
                    break;
                case LogLevel.Warn:
                    if (logger.IsWarnEnabled)
                        logger.Warn(logArgs.ReadMessage());
                    break;
                case LogLevel.Info:
                    if (logger.IsInfoEnabled)
                        logger.Info(logArgs.ReadMessage());
                    break;
                case LogLevel.Debug:
                    if (logger.IsDebugEnabled)
                        logger.Debug(logArgs.ReadMessage());
                    break;
                default:
                    logger.Warn(LogMessages.FollowingEntryHasInvalidLevel);
                    logger.Info(logArgs.ReadMessage());
                    break;
            }
        }
    }

}
