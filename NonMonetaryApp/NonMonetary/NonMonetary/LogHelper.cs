﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Globalization;
using NonMonetary.Interop;
using NonMonetary.Resources;

namespace NonMonetary
{
    internal static class LogHelper
    {
        public static void LogMethodEntry()
        {
            StackFrame callingFrame = GetStackFrame(1);
            if (callingFrame != null)
                LogMethodEntry(callingFrame.GetMethod());
        }

        public static void LogMethodEntry(
            MemberInfo memberInfo
            )
        {
            if (memberInfo == null)
                return;

            LogManager.WriteLogEntry(
                LogLevel.Debug,
                memberInfo,
                CultureInfo.CurrentCulture,
                LogMessages.EnteringMethodFormat,
                memberInfo.Name
                );
        }

        public static void LogUnhandledException(
            Exception exception
            )
        {
            StackFrame callingFrame = GetStackFrame(1);

            if (callingFrame != null)
                LogUnhandledException(exception, LogLevel.Error, callingFrame.GetMethod());
        }

        public static void LogUnhandledException(
            Exception exception,
            LogLevel level
            )
        {
            StackFrame callingFrame = GetStackFrame(1);

            if (callingFrame != null)
                LogUnhandledException(exception, level, callingFrame.GetMethod());
        }

        public static void LogUnhandledException(
            Exception exception,
            LogLevel level,
            MemberInfo memberInfo
            )
        {
            LogManager.WriteLogEntry(
                level,
                memberInfo,
                CultureInfo.CurrentCulture,
                LogMessages.UnhandledExceptionFormat,
                exception.ToString()
                );
        }

        private static StackFrame GetStackFrame(
            Int32 skipLevels
            )
        {
            StackTrace stackTrace = new StackTrace(1);
            if (stackTrace.FrameCount > skipLevels)
                return stackTrace.GetFrame(skipLevels);
            else
                return null;
        }
    }
}
