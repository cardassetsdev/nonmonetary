﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.Data;
using System.Reflection;
using System.Diagnostics;
using NonMonetary.Resources;

namespace NonMonetary
{
    public class NonMonRecord : INonMonRecord, ILogPublisher
    {
        DataRow _row;
        string _returnMessage = String.Empty;
        int _returnCode = -99;
        string _description = String.Empty;

        public NonMonRecord()
        {
            ConfigurationManager.AttachLogEntryCallback(this);
        }

        #region INonMonRecord Members

        public void AttachToRecord(DataRow row)
        {
            _row = row;
        }

        public string ReturnMessage
        {
            get
            {
                return _returnMessage;
            }
            set
            {
                _returnMessage = value;
            }
        }

        public int ReturnCode
        {
            get
            {
                return _returnCode;
            }
            set
            {
                _returnCode = value;
            }
        }

        public bool FieldValueExists(string fieldName)
        {
            return _row.Table.Columns.Contains(fieldName);
        }

        public string GetFieldValue(string fieldName)
        {
            if (!FieldValueExists(fieldName))
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    String.Format(ExceptionMessages.InvalidField, fieldName));
            }

            return (_row == null) ? String.Empty : _row[fieldName].ToString();
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        #endregion

        #region ILogPublisher Members

        private LogEntryCallback _logEntryCallback;
        protected virtual void WriteLogEntry(LogLevel level, MemberInfo info, string message)
        {
            LogEntryArgs entry = new LogEntryArgs(
                level,
                info,
                message);

            this._logEntryCallback(entry);
        }

        public void RegisterLogEntryCallback(LogEntryCallback callback)
        {
            this._logEntryCallback = callback;
        }

        #endregion
    }
}
