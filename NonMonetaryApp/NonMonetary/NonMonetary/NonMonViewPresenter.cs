﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using Microsoft.Win32;
using NonMonetary.Interface;
using System.Diagnostics;
using NonMonetary.Resources;
using System.Threading;
using System.Windows.Forms;

namespace NonMonetary
{
    public class NonMonViewPresenter : INonMonViewPresenter, ILogPublisher
    {
        private INonMonView _view;

        private INonMonPersistence _persistence = new NonMonPersistence();

        List<INonMonRecord> _records = new List<INonMonRecord>();

        public NonMonViewPresenter(INonMonView view)
        {
            this.View = view;

            ConfigurationManager.AttachLogEntryCallback(this);

            try
            {
                _persistence = new NonMonPersistence();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }
        }

        #region INonMonPresenter Members

        public INonMonView View
        {
            get
            {
                return _view;
            }
            set
            {
                SetView(value);
            }
        }

        public void Process()
        {
            try
            {
                if ((_view != null) &&
                    (_view.SelectedProcessor != null) &&
                    (!String.IsNullOrEmpty(_view.FileName)))
                {
                    if (_view.SelectedProcessor.Status == NonMonProcessingStatus.Paused)
                        Continue();
                    else if (_view.SelectedProcessor.Status == NonMonProcessingStatus.InProcess)
                        Pause();
                    else
                    {
                        //!!!!!!!!!!!!!!
                        _persistence.Initialize(_view.SelectedProcessor);

                        _view.SelectedProcessor.OnProgressChanged += new EventHandler<ProgressChangedEventArgs>(this.ProgressChanged);

                        _view.SelectedProcessor.OnStatusChanged += new EventHandler(this.ViewProcessorStatusChanged);

                        _view.SelectedProcessor.OnRecordCompleted += new EventHandler<NonMonRecordArgs>(this.ViewRecordCompleted);

                        NonMonRecordLoader loader = new NonMonRecordLoader();

                        _records = loader.LoadRecords(_view.FileName);

                        _view.SelectedProcessor.ProcessNonMonRecords(_records);
                    }
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }
        }

        public void LoadNonMonProcessors()
        {
            try
            {
                if (this._view != null)
                {
                    NonMonProcessorLoader loader = new NonMonProcessorLoader();

                    _view.LoadNonMonList(loader.LoadProcessors(Application.StartupPath));
                }

                _persistence = new NonMonPersistence();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }
        }

        public void Pause()
        {
            if ((_view.SelectedProcessor != null) &&
                (_view.SelectedProcessor.Status == NonMonProcessingStatus.InProcess))
                _view.SelectedProcessor.Pause();
        }

        public void Abort()
        {
            if ((_view.SelectedProcessor != null) &&
                (_view.SelectedProcessor.Status == NonMonProcessingStatus.InProcess))
                _view.SelectedProcessor.Abort();
        }

        public void Continue()
        {
            if ((_view.SelectedProcessor != null) &&
                (_view.SelectedProcessor.Status == NonMonProcessingStatus.Paused))
                _view.SelectedProcessor.Continue();
        }

        #endregion

        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _view.SetProgress(
                this, e);

            if (e.ProgressPercentage == 100)
            {
                _view.SelectedProcessor.OnProgressChanged -= this.ProgressChanged;
                _view.SelectedProcessor.OnStatusChanged -= this.ViewProcessorStatusChanged;
            }
        }

        private void SetView(INonMonView view)
        {
            try
            {
                if (view == null)
                    throw new Exception(ExceptionMessages.InvalidView);

                if (this._view != null)
                {
                    this._view.OnProcessorChanged -= this.ViewProcessorChanged;
                    this._view.OnFileSelected -= this.ViewFileSelected;
                }

                this._view = view;
                this._view.OnProcessorChanged += new EventHandler(this.ViewProcessorChanged);
                this._view.OnFileSelected += new EventHandler(this.ViewFileSelected);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }
        }

        private void ViewProcessorChanged(object sender, EventArgs e)
        {
            if (this._view != null)
            {
                this._view.CanExecute =
                    (!String.IsNullOrEmpty(this._view.FileName)) &&
                    (this._view.SelectedProcessor != null);
            }
        }

        private void ViewProcessorStatusChanged(object sender, EventArgs e)
        {
            SetViewContentFromProcessorStatus();
        }

        private void ViewRecordCompleted(object sender, NonMonRecordArgs e)
        {
            try
            {
                _persistence.AddRecord(e.Record);
                _persistence.Persist();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }
        }

        private void ViewFileSelected(object sender, EventArgs e)
        {
            if (this._view != null)
            {
                this._view.CanExecute = 
                    (!String.IsNullOrEmpty(this._view.FileName)) &&
                    (this._view.SelectedProcessor != null);
            }
        }

        private void SetViewContentFromProcessorStatus()
        {
            try
            {
                if ((this._view != null) &&
                    (this._view.SelectedProcessor != null))
                {
                    switch (this._view.SelectedProcessor.Status)
                    {
                        case NonMonProcessingStatus.Aborted:
                            {
                                this._view.CanAbort = false;
                                this._view.CanExecute = false;
                                break;
                            }
                        case NonMonProcessingStatus.InProcess:
                            {
                                this._view.CanAbort = true;
                                this._view.CanSelectFile = false;
                                this._view.CanSelectProcessor = false;
                                this._view.ExecuteContent = Text.Pause;
                                break;
                            }
                        case NonMonProcessingStatus.Paused:
                            {
                                this._view.CanAbort = false;
                                this._view.CanSelectFile = false;
                                this._view.CanSelectProcessor = false;
                                this._view.ExecuteContent = Text.Continue;
                                break;
                            }
                        case NonMonProcessingStatus.Completed:
                            {
                                this._view.CanAbort = false;
                                this._view.CanSelectFile = true;
                                this._view.CanSelectProcessor = true;
                                this._view.ExecuteContent = Text.Execute;
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }
        }

        #region ILogPublisher Members

        private LogEntryCallback _logEntryCallback;
        protected virtual void WriteLogEntry(LogLevel level, MemberInfo info, string message)
        {
            LogEntryArgs entry = new LogEntryArgs(
                level,
                info,
                message);

            this._logEntryCallback(entry);
        }

        public void RegisterLogEntryCallback(LogEntryCallback callback)
        {
            this._logEntryCallback = callback;
        }

        #endregion

        #region INonMonViewPresenter Members

        public List<INonMonRecord> Records
        {
            get { return this._records; }
        }

        #endregion
    }
}
