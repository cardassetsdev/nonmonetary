﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;
using NonMonetary.Resources;

namespace NonMonetary
{
    public class NonMonPersistence : INonMonPersistence, ILogPublisher
    {
        NonMonetaryEntities _entities;
        NonMonetaryBatch _batch;

        #region INonMonPersistence Members

        public string FoodPreference(string eaterType)
        {
            string retVal;

            switch (eaterType)
            {
                case "BeefLover":
                    retVal = "Steak";
                    break;
                case "PoultryLover":
                    retVal = "Chicken";
                    break;
                case "Vegetarian":
                case "Vegan":
                    retVal = "Eggplant";
                    break;
                default:
                    retVal = "Undecided";
                    break;
            }

            return retVal;
        }


        public NonMonPersistence()
        {
            ConfigurationManager.AttachLogEntryCallback(this);
        }

        public void AddRecord(INonMonRecord record)
        {
            if (record == null)
            {
                StackTrace stackTrace = new StackTrace(1);
                this.WriteLogEntry(
                  LogLevel.Fatal,
                  stackTrace.GetFrame(0).GetMethod(),
                  ExceptionMessages.InvalidRecord);

                return;
            }

            NonMonetaryTransaction trans = new NonMonetaryTransaction();
            trans.AccountNumber = record.GetFieldValue(Fields.AccountNumber);
            trans.Description = record.Description;
            trans.ReturnMessage = record.ReturnMessage;
            trans.ReturnCode = record.ReturnCode;
            trans.RunDateTime = DateTime.Now;
            trans.NonMonetaryBatch = _batch;
            _entities.AddToNonMonetaryTransaction(trans);
            _entities.SaveChanges(true);
        }

        public void Initialize(INonMonProcessor processor)
        {
            if (processor == null)
            {
                StackTrace stackTrace = new StackTrace(1);
                this.WriteLogEntry(
                  LogLevel.Fatal,
                  stackTrace.GetFrame(0).GetMethod(),
                  ExceptionMessages.InvalidProcessor);

                return;
            }

            _entities = new NonMonetaryEntities(System.Configuration.ConfigurationManager.ConnectionStrings[Resources.Attributes.ConnectionStringName].ConnectionString);
            _batch = new NonMonetaryBatch();
            _batch.NonMonetaryDescription = processor.ProcessorName;
            _batch.RunDate = DateTime.Now;
            _entities.AddToNonMonetaryBatch(_batch);
            _entities.SaveChanges();        
        }

        public void Persist()
        {
            _entities.SaveChanges(true);
        }

        #endregion

        #region ILogPublisher Members

        private LogEntryCallback _logEntryCallback;
        protected virtual void WriteLogEntry(LogLevel level, MemberInfo info, string message)
        {
            LogEntryArgs entry = new LogEntryArgs(
                level,
                info,
                message);

            this._logEntryCallback(entry);
        }

        public void RegisterLogEntryCallback(LogEntryCallback callback)
        {
            this._logEntryCallback = callback;
        }

        #endregion
    }
}
