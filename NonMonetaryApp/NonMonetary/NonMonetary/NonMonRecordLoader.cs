﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.IO;
using System.Data;
using System.Data.Odbc;
using System.Diagnostics;
using System.Reflection;
using NonMonetary.Resources;
using System.Data.OleDb;

namespace NonMonetary
{
    public class NonMonRecordLoader : INonMonRecordLoader, ILogPublisher
    {
        public NonMonRecordLoader()
        {
            ConfigurationManager.AttachLogEntryCallback(this);
        }

        #region INonMonRecordLoader Members

        public List<INonMonRecord> LoadRecords(string fileName)
        {
            List<INonMonRecord> records = new List<INonMonRecord>();

            try
            {
                if (File.Exists(fileName))
                {
                    FileInfo fileInfo = new FileInfo(fileName);
                    try
                    {
                        DataTable tbl = new DataTable();

                        using (OdbcConnection cn = new OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)}; DBQ=[" + fileInfo.DirectoryName + "]"))
                        {
                            string sqlStatement = String.Format(SQL.ExcelSQLStatement, fileInfo.Name);

                            using (OdbcCommand cmd = new OdbcCommand(sqlStatement, cn))
                            {
                                using (OdbcDataAdapter adapter = new OdbcDataAdapter(cmd))
                                {
                                    cn.Open();

                                    adapter.Fill(tbl);
                                }
                            }
                        }


                        INonMonRecord record;

                        foreach (DataRow row in tbl.Rows)
                        {
                            record = new NonMonRecord();
                            record.AttachToRecord(row);
                            records.Add(record);
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message);
            }

            return records;
        }

        #endregion

        #region ILogPublisher Members

        private LogEntryCallback _logEntryCallback;
        protected virtual void WriteLogEntry(LogLevel level, MemberInfo info, string message)
        {
            LogEntryArgs entry = new LogEntryArgs(
                level,
                info,
                message);

            this._logEntryCallback(entry);
        }

        public void RegisterLogEntryCallback(LogEntryCallback callback)
        {
            this._logEntryCallback = callback;
        }

        #endregion
    }
}
