﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.Xml;
using NonMonetary.Resources;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Configuration;

namespace NonMonetary
{
    public static class ConfigurationManager
    {
        internal static XPathExpression CompileConfigurationXPath(
            String expression
            )
        {
            LogHelper.LogMethodEntry();

            NameTable nameTable = new NameTable();
            XmlNamespaceManager nsManager = new XmlNamespaceManager(nameTable);
            nsManager.AddNamespace(
                XPath.NonMonetaryConfigurationNamespacePrefix,
                XPath.NonMonetaryConfigurationNamespace
                );

            return XPathExpression.Compile(expression, nsManager);
        }

        internal static String GetAttributeValue(
            IXPathNavigable iXPathNavigable,
            String attributeName
            )
        {
            LogHelper.LogMethodEntry();

            if (iXPathNavigable == null)
                return null;

            XPathNavigator navigator = iXPathNavigable.CreateNavigator();

            if (
                navigator.MoveToAttribute(attributeName, null)
                || navigator.MoveToAttribute(attributeName, String.Empty)
                || navigator.MoveToAttribute(attributeName, navigator.NamespaceURI)
                )
                return navigator.Value;
            else
                return null;
        }

        internal static IEnumerable<IXPathNavigable> SelectNodes(
            String expression,
            XPathNavigator nav
            )
        {
            LogHelper.LogMethodEntry();

            if (expression == null)
                throw new ArgumentNullException(Attributes.Expression);

            return SelectNodes(
                CompileConfigurationXPath(expression),
                nav
                );
        }

        private static IEnumerable<IXPathNavigable> SelectNodes(
            XPathExpression xPathExpression,
            XPathNavigator nav
            )
        {
            LogHelper.LogMethodEntry();

            List<IXPathNavigable> nodes = new List<IXPathNavigable>();

            foreach (IXPathNavigable assemblyNav in nav.Select(xPathExpression))
                nodes.Add(assemblyNav);

            return nodes.ToArray();
        }

        internal static XPathDocument LoadConfigurationDocument(
            String configPath
            )
        {
            LogHelper.LogMethodEntry();

            try
            {
                if (String.IsNullOrEmpty(configPath))
                    throw new ConfigurationErrorsException(ExceptionMessages.InvalidConfigPath);

                XPathDocument configDoc = new XPathDocument(configPath);
                return configDoc;
            }
            catch
            {
                System.Configuration.ConfigurationManager.RefreshSection(Attributes.AppSettings);
                throw;
            }
        }

        public static void AttachLogEntryCallback(
            Object loadedObject
            )
        {
            LogHelper.LogMethodEntry();

            ILogPublisher logSubscriber = loadedObject as ILogPublisher;
            if (logSubscriber != null)
                logSubscriber.RegisterLogEntryCallback(LogManager.WriteLogEntry);
        }
    }
}
