﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.ComponentModel;
using NonMonetary;
using NonMonetary.Interop;
using System.Windows.Threading;
using System.Threading;
using NonMonetary.Interface;
using System.Reflection;
using Tomers.WPF.Themes.Skins;
using System.Diagnostics;
using NonMonetary.Resources;

namespace NonMonetary
{
    /// <summary>
    /// Interaction logic for WindowNonMon.xaml
    /// </summary>
    public partial class WindowNonMon : Window, INonMonView
    {
        // Using a DependencyProperty as the backing store for CurrentSkin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentSkinProperty =
            DependencyProperty.Register(
                "CurrentSkin",
                typeof(Skin),
                typeof(WindowNonMon),
                new UIPropertyMetadata(Skin.Null, OnCurrentSkinChanged, OnCoerceSkinValue));

        private static DispatcherOperationCallback exitFrameCallback = new
                                DispatcherOperationCallback(ExitFrame);

        INonMonViewPresenter _presenter;

        public WindowNonMon()
        {
            InitializeComponent();

            SetValue(CurrentSkinProperty, new AppDomainAssemblySkin("Aero", new AssemblyName("PresentationFramework.Aero, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35")));

            _presenter = new NonMonViewPresenter(this);

            _presenter.LoadNonMonProcessors();
        }

        private void buttonSelectFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.CheckFileExists = true;
            dlg.Filter = "Comma Delimited Files (.txt)|*.txt;*.csv";
            bool? dlgExecutedSuccessfully = dlg.ShowDialog(this);

            if ((dlgExecutedSuccessfully.HasValue) && (dlgExecutedSuccessfully.Value))
            {
                FileName = dlg.FileName;

                if (this.OnFileSelected != null)
                    this.OnFileSelected(this, new EventArgs());
            }
        }

        #region INonMonView Members
        
        public string AbortContent
        {
            get
            {
                return (string)this.buttonAbort.Content;
            }
            set
            {
                this.buttonAbort.Content = value;
            }
        }

        public bool CanAbort
        {
            get
            {
                return this.buttonAbort.IsEnabled;
            }
            set
            {
                this.buttonAbort.IsEnabled = value;
            }
        }

        public bool CanExecute
        {
            get
            {
                return this.buttonExecute.IsEnabled;
            }
            set
            {
                this.buttonExecute.IsEnabled = value;
            }
        }

        public bool CanSelectFile
        {
            get
            {
                return (this.buttonSelectFile.IsEnabled &&
                    this.textBoxFileToProcess.IsEnabled);
            }
            set
            {
                this.buttonSelectFile.IsEnabled = value;
                this.textBoxFileToProcess.IsEnabled = value;
            }
        }

        public bool CanSelectProcessor
        {
            get
            {
                return this.comboBoxNonMon.IsEnabled;
            }
            set
            {
                this.comboBoxNonMon.IsEnabled = value;
            }
        }

        public string ExecuteContent
        {
            get
            {
                return (string)this.buttonExecute.Content;
            }
            set
            {
                this.buttonExecute.Content = value;
            }
        }

        public string FileName
        {
            get
            {
                return textBoxFileToProcess.Text;
            }
            set
            {
                textBoxFileToProcess.Text = value;
            }
        }

        public INonMonViewPresenter Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                _presenter = value;
            }
        }

        public INonMonProcessor SelectedProcessor
        {
            get { return comboBoxNonMon.SelectedIndex < 0 ? null : (comboBoxNonMon.SelectedItem as INonMonProcessor); }
        }

        public EventHandler OnFileSelected { get; set; }

        public EventHandler OnProcessorChanged { get; set; }

        public void SetProgress(object sender, ProgressChangedEventArgs e)
        {
            progressBarProgress.Minimum = 0;
            progressBarProgress.Maximum = 100;
            progressBarProgress.Value = e.ProgressPercentage;

            if (e.UserState is string)
                this.textBlockProgress.Text = (string)e.UserState;

            DoEvents();
        }

        public void LoadNonMonList(List<INonMonProcessor> list)
        {
            comboBoxNonMon.Items.Clear();

            comboBoxNonMon.DisplayMemberPath = "ProcessorName";

            foreach (INonMonProcessor processor in list)
                comboBoxNonMon.Items.Add(processor);
        }

        #endregion

        private void buttonExecute_Click(object sender, RoutedEventArgs e)
        {
            _presenter.Process();
        }

        private void buttonAbort_Click(object sender, RoutedEventArgs e)
        {
            _presenter.Abort();
        }

        /// <summary>
        /// Processes all UI messages currently in the message queue.
        /// </summary>
        private static void DoEvents()
        {
            // Create new nested message pump.
            DispatcherFrame nestedFrame = new DispatcherFrame();

            // Dispatch a callback to the current message queue, when getting called, 
            // this callback will end the nested message loop.
            // note that the priority of this callback should be lower than the that of UI event messages.
            DispatcherOperation exitOperation = Dispatcher.CurrentDispatcher.BeginInvoke(
                                                  DispatcherPriority.Background,
                                                  exitFrameCallback,
                                                  nestedFrame);

            // pump the nested message loop, the nested message loop will 
            // immediately process the messages left inside the message queue.
            Dispatcher.PushFrame(nestedFrame);

            // If the "exitFrame" callback doesn't get finished, Abort it.
            if (exitOperation.Status != DispatcherOperationStatus.Completed)
            {
                exitOperation.Abort();
            }
        }

        private static Object ExitFrame(Object state)
        {
            DispatcherFrame frame = state as DispatcherFrame;

            // Exit the nested message loop.
            frame.Continue = false;

            return null;

        }

        private void comboBoxNonMon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OnProcessorChanged != null)
                OnProcessorChanged(sender, e);
        }

        private static void OnCurrentSkinChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                //Skin oldSkin = e.OldValue as Skin;
                //oldSkin.Unload();
                //Skin newSkin = e.NewValue as Skin;

                //newSkin.Load();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Change error: " + ex.Message);
                throw;
            }
        }

        private static object OnCoerceSkinValue(DependencyObject d, object baseValue)
        {
            try
            {
                if (baseValue == null)
                {
                    return Skin.Null;
                }
                return baseValue;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Coerce error: " + ex.Message);
                throw;
            }
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void WindowNonMon_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }
    }
}
