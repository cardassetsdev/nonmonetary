using System;
using System.Collections.Generic;
using System.Text;
using NonMonetary.Interop;
using System.Configuration;
using CardAssets.TP;
using System.ComponentModel;
using log4net;
using NonMonetary.Implementations.TryTransferCompany.Resources;
using System.Diagnostics;
using System.Xml.XPath;

namespace NonMonetary.Implementations.TryTransferCompany
{
    public class TryTransferCompany : INonMonProcessor
    {
        string _processorName;

        NonMonProcessingStatus _status = NonMonProcessingStatus.Pending;

        int _index = 0;

        List<INonMonRecord> _records;

        #region INonMonProcessor Members

        public event EventHandler<ProgressChangedEventArgs> OnProgressChanged;

        public event EventHandler<NonMonRecordArgs> OnRecordCompleted;

        public event EventHandler OnStatusChanged;

        public void Configure(IXPathNavigable xPathNavigable)
        {
        }

        public void ProcessNonMonRecords(List<INonMonRecord> records)
        {
            try
            {
                _index = 0;

                _records = records;

                Process();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private static void WriteLogEntry(
            LogEntryArgs logArgs
            )
        {
            Type declaringType = logArgs.MemberInfo.DeclaringType;

            log4net.ILog logger = log4net.LogManager.GetLogger(
                declaringType != null
                ? declaringType
                : logArgs.MemberInfo as Type
                );

            switch (logArgs.LogLevel)
            {
                case LogLevel.Fatal:
                    if (logger.IsFatalEnabled)
                        logger.Fatal(logArgs.ReadMessage());
                    break;
                case LogLevel.Error:
                    if (logger.IsErrorEnabled)
                        logger.Error(logArgs.ReadMessage());
                    break;
                case LogLevel.Warn:
                    if (logger.IsWarnEnabled)
                        logger.Warn(logArgs.ReadMessage());
                    break;
                case LogLevel.Info:
                    if (logger.IsInfoEnabled)
                        logger.Info(logArgs.ReadMessage());
                    break;
                case LogLevel.Debug:
                    if (logger.IsDebugEnabled)
                        logger.Debug(logArgs.ReadMessage());
                    break;
                default:
                    logger.Warn(LogMessages.FollowingEntryHasInvalidLevel);
                    logger.Info(logArgs.ReadMessage());
                    break;
            }
        }

        public void PerformNonMon(INonMonRecord item, TPConnector connector)
        {
            try
            {               

                item.Description = String.Format(
                    "Transferring company record, {0} ",
                    TrimString(item.GetFieldValue(Resources.Fields.CompanyID)));

                connector.resetInput();
                connector.addInput(Resources.Fields.SysPrinAgent, TrimString(item.GetFieldValue(Resources.Fields.SysPrinAgent)));
                connector.addInput(Resources.Fields.CompanyID, TrimString(item.GetFieldValue(Resources.Fields.CompanyID)));

                item.ReturnCode = connector.runTransaction(Resources.Fields.TRANS_TRY_TRANSFER_COMPANY);
                item.ReturnMessage = TrimString(connector.getOutput(Resources.Fields.ReturnMessage));
                var companyname = TrimString(connector.getOutput(Resources.Fields.CompanyName));

                if (item.ReturnCode != 0)
                    return;

                if (!String.IsNullOrEmpty(companyname))
                {
                    item.ReturnMessage = "Company already exists.";
                    return;
                }

                connector.resetInput();
                connector.addInput(Resources.Fields.SystemID, TrimString(item.GetFieldValue(Resources.Fields.SystemID)));
                connector.addInput(Resources.Fields.Prin, TrimString(item.GetFieldValue(Resources.Fields.Prin)));
                connector.addInput(Resources.Fields.BankID, TrimString(item.GetFieldValue(Resources.Fields.BankID)));
                connector.addInput(Resources.Fields.OldAccountNo, "");
                item.ReturnCode = connector.runTransaction(Resources.Fields.TRANS_CREATE_NEW_CREDIT_CARD_ACCOUNT);

                if (item.ReturnCode == 0)
                {
                    StringBuilder sb = new StringBuilder();

                    item.ReturnMessage = TrimString(connector.getOutput(Resources.Fields.ReturnMessage));
                    var _newAccountNumber = TrimString(connector.getOutput(Resources.Fields.NewAccountNo));

                    sb.Append(item.GetFieldValue(Resources.Fields.CompanyID) + "," + _newAccountNumber);

                    connector.resetInput();
                    item.ReturnCode = connector.runTransaction(Resources.Fields.TRANS_CREATE_NEW_CREDIT_CARD_ACCOUNT_PARK);

                    if (item.ReturnCode == 0)
                    {

                        connector.resetInput();

                        connector.addInput(Resources.Fields.SysPrinAgent, TrimString(item.GetFieldValue(Resources.Fields.SysPrinAgent)));
                        connector.addInput(Resources.Fields.CompanyID, TrimString(item.GetFieldValue(Resources.Fields.CompanyID)));
                        connector.addInput(Resources.Fields.CompanyName, TrimString(item.GetFieldValue(Resources.Fields.CompanyName)));
                        connector.addInput(Resources.Fields.PrincipalOfficer, TrimString(item.GetFieldValue(Resources.Fields.PrincipalOfficer)));
                        connector.addInput(Resources.Fields.FiscalYear, TrimString(item.GetFieldValue(Resources.Fields.FiscalYear)));
                        connector.addInput(Resources.Fields.Misc1, TrimString(item.GetFieldValue(Resources.Fields.Misc1)));
                        connector.addInput(Resources.Fields.Misc2, TrimString(item.GetFieldValue(Resources.Fields.Misc2)));
                        connector.addInput(Resources.Fields.Misc3, TrimString(item.GetFieldValue(Resources.Fields.Misc3)));
                        connector.addInput(Resources.Fields.Misc4, TrimString(item.GetFieldValue(Resources.Fields.Misc4)));
                        connector.addInput(Resources.Fields.AnnualFeeAmount, "0");
                        connector.addInput(Resources.Fields.Inactivity, "06");
                        connector.addInput(Resources.Fields.ReportFee, "002");
                        connector.addInput(Resources.Fields.BillingAccountNo, _newAccountNumber);
                        connector.addInput(Resources.Fields.BusType, TrimString(item.GetFieldValue(Resources.Fields.BusType)));
                        connector.addInput(Resources.Fields.FeeOption, TrimString(item.GetFieldValue(Resources.Fields.FeeOption)));
                        connector.addInput(Resources.Fields.CompanyContact, TrimString(item.GetFieldValue(Resources.Fields.CompanyContact)));
                        connector.addInput(Resources.Fields.ContactTelephone, TrimString(item.GetFieldValue(Resources.Fields.ContactTelephone)));
                        connector.addInput(Resources.Fields.NextReviewDate, TrimString(item.GetFieldValue(Resources.Fields.NextReviewDate)));
                        connector.addInput(Resources.Fields.Officer, TrimString(item.GetFieldValue(Resources.Fields.Officer)));
                        connector.addInput(Resources.Fields.CreditScore, TrimString(item.GetFieldValue(Resources.Fields.CreditScore)));
                        connector.addInput(Resources.Fields.CreditLimit, TrimString(item.GetFieldValue(Resources.Fields.CreditLimit)));
                        connector.addInput(Resources.Fields.CreditPercent, "110");
                        connector.addInput(Resources.Fields.BalanceLinePercent, "090");
                        connector.addInput(Resources.Fields.BalancePercentAverage, "090");
                        connector.addInput(Resources.Fields.NatureOfBusiness, TrimString(item.GetFieldValue(Resources.Fields.NatureOfBusiness)));
                        connector.addInput(Resources.Fields.CycleCode, TrimString(item.GetFieldValue(Resources.Fields.CycleCode)));
                        connector.addInput(Resources.Fields.YearEstablished, TrimString(item.GetFieldValue(Resources.Fields.YearEstablished)));
                        connector.addInput(Resources.Fields.NoEmployees, TrimString(item.GetFieldValue(Resources.Fields.NoEmployees)));
                        connector.addInput(Resources.Fields.AddressFormatCode, TrimString(item.GetFieldValue(Resources.Fields.AddressFormatCode)));
                        connector.addInput(Resources.Fields.ContractExp, TrimString(item.GetFieldValue(Resources.Fields.ContractExp)));
                        connector.addInput(Resources.Fields.AddressLine1, TrimString(item.GetFieldValue(Resources.Fields.AddressLine1)));
                        connector.addInput(Resources.Fields.AddressLine2, TrimString(item.GetFieldValue(Resources.Fields.AddressLine2)));
                        connector.addInput(Resources.Fields.AddressLine3, TrimString(item.GetFieldValue(Resources.Fields.AddressLine3)));
                        connector.addInput(Resources.Fields.City, TrimString(item.GetFieldValue(Resources.Fields.City)));
                        connector.addInput(Resources.Fields.State, TrimString(item.GetFieldValue(Resources.Fields.State)));
                        connector.addInput(Resources.Fields.Zipcode, TrimString(item.GetFieldValue(Resources.Fields.Zipcode)));
                        connector.addInput(Resources.Fields.Country, TrimString(item.GetFieldValue(Resources.Fields.Country)));

                        item.ReturnCode = connector.runTransaction(Resources.Fields.TRANS_TRANSFER_COMPANY_RECORD);


                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\temp\TransferCompanyResults.txt", true))
                        {
                            file.WriteLine(sb.ToString());
                        }

                        if (item.ReturnCode == 0)
                        {
                            item.ReturnMessage = TrimString(connector.getOutput(Resources.Fields.ReturnMessage));
                        }
                        else
                            item.ReturnMessage = "Board Company Failed";
                    }
                    else
                        item.ReturnMessage = "Create New Credit Card Account Park Failed";
                }
                else
                    item.ReturnMessage = "Create New Credit Card Account Failed";
            
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        public string ProcessorName
        {
            get { return _processorName; }
            set { _processorName = value; }
        }

        public void Abort()
        {
            this._status = NonMonProcessingStatus.Aborted;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public void Continue()
        {
            if (_status == NonMonProcessingStatus.Paused)
            {
                Process();
            }
        }

        public void Pause()
        {
            this._status = NonMonProcessingStatus.Paused;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public NonMonProcessingStatus Status
        {
            get { return _status; }
        }

        #endregion

        private void Process()
        {
            try
            {
                _status = NonMonProcessingStatus.InProcess;

                if (OnStatusChanged != null)
                    OnStatusChanged(this, new EventArgs());

                int max = _records.Count;

                string host = ConfigurationManager.AppSettings.Get(Resources.Fields.CleoHost);
                int port = Convert.ToInt32(ConfigurationManager.AppSettings.Get(Resources.Fields.CleoPort));

                TPConnector connector = new TPConnector(host, port);
                try
                {
                    if (connector.Reserve(System.Configuration.ConfigurationManager.AppSettings.Get(Resources.Fields.TransactionSetName)) == 0)
                    {
                        INonMonRecord record;

                        while (_index < max)
                        {
                            record = _records[_index];

                            if ((_status == NonMonProcessingStatus.Aborted) ||
                                (_status == NonMonProcessingStatus.Paused))
                                break;

                            if (this.OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        (_index * 100) / max,
                                        String.Format("Processing {0} of {1}", _index + 1, max)));

                            PerformNonMon(record, connector);

                            if (this.OnRecordCompleted != null)
                                this.OnRecordCompleted(this, new NonMonRecordArgs(record));

                            _index++;
                        }

                        if (_index == max)
                        {
                            this._status = NonMonProcessingStatus.Completed;

                            if (OnStatusChanged != null)
                                OnStatusChanged(this, new EventArgs());

                            if (OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        100,
                                        String.Format(
                                            "{0} records processed", 
                                            max)));
                        }
                    }
                }
                finally
                {
                    connector.Release(true);
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private string TrimString(string inputString)
        {
            return (inputString == null) ? String.Empty : inputString.Trim();
        }
    }
}
