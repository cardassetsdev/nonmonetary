using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.Configuration;
using CardAssets.TP;
using System.ComponentModel;
using System.Threading;
using log4net;
using NonMonetary.Implementations.BoardAccount.Resources;
using System.Reflection;
using System.Diagnostics;
using System.Xml.XPath;

namespace NonMonetary.Implementations.BoardAccount
{
    public class BoardAccount : INonMonProcessor
    {
        string _processorName;

        NonMonProcessingStatus _status = NonMonProcessingStatus.Pending;

        int _index = 0;

        List<INonMonRecord> _records;

        #region INonMonProcessor Members

        public event EventHandler<ProgressChangedEventArgs> OnProgressChanged;

        public event EventHandler<NonMonRecordArgs> OnRecordCompleted;

        public event EventHandler OnStatusChanged;

        public void Configure(IXPathNavigable xPathNavigable)
        {
        }

        public void ProcessNonMonRecords(List<INonMonRecord> records)
        {
            try
            {
                _index = 0;

                _records = records;

                Process();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private static void WriteLogEntry(
            LogEntryArgs logArgs
            )
        {
            Type declaringType = logArgs.MemberInfo.DeclaringType;

            log4net.ILog logger = log4net.LogManager.GetLogger(
                declaringType != null
                ? declaringType
                : logArgs.MemberInfo as Type
                );

            switch (logArgs.LogLevel)
            {
                case LogLevel.Fatal:
                    if (logger.IsFatalEnabled)
                        logger.Fatal(logArgs.ReadMessage());
                    break;
                case LogLevel.Error:
                    if (logger.IsErrorEnabled)
                        logger.Error(logArgs.ReadMessage());
                    break;
                case LogLevel.Warn:
                    if (logger.IsWarnEnabled)
                        logger.Warn(logArgs.ReadMessage());
                    break;
                case LogLevel.Info:
                    if (logger.IsInfoEnabled)
                        logger.Info(logArgs.ReadMessage());
                    break;
                case LogLevel.Debug:
                    if (logger.IsDebugEnabled)
                        logger.Debug(logArgs.ReadMessage());
                    break;
                default:
                    logger.Warn(LogMessages.FollowingEntryHasInvalidLevel);
                    logger.Info(logArgs.ReadMessage());
                    break;
            }
        }

        public void PerformNonMon(INonMonRecord item, TPConnector connector)
        {
            try
            {
                if (!item.FieldValueExists(Resources.Fields.AccountNumber))
                    throw new Exception(LogMessages.RecordMissingFields);

                StringBuilder sb = new StringBuilder();

                //item.Description = String.Format(
                //    "Account {0} was closed",
                //    TrimString(item.GetFieldValue(Resources.Fields.AccountNumber)));

                connector.resetInput();
                
                connector.addInput(Resources.Fields.SystemID, TrimString(item.GetFieldValue(Resources.Fields.SystemID)));
                connector.addInput(Resources.Fields.Prin, TrimString(item.GetFieldValue(Resources.Fields.Prin)));
                connector.addInput(Resources.Fields.BankID, TrimString(item.GetFieldValue(Resources.Fields.BankID)));

                connector.addInput(Resources.Fields.CreditScore, TrimString(item.GetFieldValue(Resources.Fields.CreditScore)));
                connector.addInput(Resources.Fields.PlasticType, TrimString(item.GetFieldValue(Resources.Fields.PlasticType)));
                connector.addInput(Resources.Fields.PrimaryBirthdate, TrimString(item.GetFieldValue(Resources.Fields.PrimaryBirthdate)));                
                connector.addInput(Resources.Fields.TP, TrimString(item.GetFieldValue(Resources.Fields.TP)));
                connector.addInput(Resources.Fields.CreditLimit, TrimString(item.GetFieldValue(Resources.Fields.CreditLimit)));
                connector.addInput(Resources.Fields.RushIndicator, TrimString(item.GetFieldValue(Resources.Fields.RushIndicator)));
                connector.addInput(Resources.Fields.PrimaryName, TrimString(item.GetFieldValue(Resources.Fields.PrimaryName)));
                connector.addInput(Resources.Fields.PrimarySSN, TrimString(item.GetFieldValue(Resources.Fields.PrimarySSN)));
                connector.addInput(Resources.Fields.City, TrimString(item.GetFieldValue(Resources.Fields.City)));
                connector.addInput(Resources.Fields.AddressLine1, TrimString(item.GetFieldValue(Resources.Fields.AddressLine1)));
                connector.addInput(Resources.Fields.AddressLine2, TrimString(item.GetFieldValue(Resources.Fields.AddressLine2)));                
                connector.addInput(Resources.Fields.State, TrimString(item.GetFieldValue(Resources.Fields.State)));
                connector.addInput(Resources.Fields.Zipcode, TrimString(item.GetFieldValue(Resources.Fields.Zipcode)));
                connector.addInput(Resources.Fields.Phone1, TrimString(item.GetFieldValue(Resources.Fields.Phone1)));
                connector.addInput(Resources.Fields.Phone2, TrimString(item.GetFieldValue(Resources.Fields.Phone2)));
                connector.addInput(Resources.Fields.CBFlag, TrimString(item.GetFieldValue(Resources.Fields.CBFlag)));
                connector.addInput(Resources.Fields.Misc1, TrimString(item.GetFieldValue(Resources.Fields.Misc1)));
                connector.addInput(Resources.Fields.Misc2, TrimString(item.GetFieldValue(Resources.Fields.Misc2)));
                connector.addInput(Resources.Fields.Misc3, TrimString(item.GetFieldValue(Resources.Fields.Misc3)));
                connector.addInput(Resources.Fields.Misc4, TrimString(item.GetFieldValue(Resources.Fields.Misc4)));
                connector.addInput(Resources.Fields.NumberOfPlastics, TrimString(item.GetFieldValue(Resources.Fields.NumberOfPlastics)));
                connector.addInput(Resources.Fields.CompanyID, TrimString(item.GetFieldValue(Resources.Fields.CompanyID)));
                connector.addInput(Resources.Fields.OpenDate,TrimString(item.GetFieldValue(Resources.Fields.OpenDate)));
                connector.addInput(Resources.Fields.AddressFormatCode,"F");
                connector.addInput(Resources.Fields.Country, TrimString(item.GetFieldValue(Resources.Fields.Country)));

                item.ReturnCode = connector.runTransaction(Resources.Fields.BoardNewCreditCardAccount);

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\temp\BoardAccount_results.txt", true))
                {
                    item.ReturnMessage = "BoardNewCreditCardAccount " + TrimString(connector.getOutput(Resources.Fields.ReturnMessage));
                    item.Description = TrimString(connector.getOutput(Resources.Fields.NewAccountNo));

                    sb.Append(item.GetFieldValue(Resources.Fields.AccountNumber) + "," + TrimString(connector.getOutput(Resources.Fields.NewAccountNo)) + "," + item.ReturnMessage);

                    if (item.ReturnCode == 0)
                    {
                        connector.resetInput();

                        connector.addInput(Resources.Fields.AccountNumber, TrimString(connector.getOutput(Resources.Fields.NewAccountNo)));
                        connector.addInput(Resources.Fields.PricingStrategy, TrimString(item.GetFieldValue(Resources.Fields.PricingStrategy)));

                        item.ReturnCode = connector.runTransaction(Resources.Fields.SetAccountPricingStrategy);

                        if (item.ReturnCode == 0)
                        {
                            item.ReturnMessage += "SetPricingStrategy " + TrimString(connector.getOutput(Resources.Fields.ReturnMessage));
                            item.ReturnCode = connector.runTransaction(Resources.Fields.SetAccountPricingStrategyPark);
                        }
                        else
                            throw new Exception("SetPricingStrategy Transaction Failed");
                    }
                    else
                    {

                        item.ReturnMessage = "BoardNewCreditCardAccount Transaction Failed";
                    }        
        
                    file.WriteLine(sb.ToString());
                } 

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        public string ProcessorName
        {
            get { return _processorName; }
            set { _processorName = value; }
        }

        public void Abort()
        {
            this._status = NonMonProcessingStatus.Aborted;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public void Continue()
        {
            if (_status == NonMonProcessingStatus.Paused)
            {
                Process();
            }
        }

        public void Pause()
        {
            this._status = NonMonProcessingStatus.Paused;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public NonMonProcessingStatus Status
        {
            get { return _status; }
        }

        #endregion

        private void Process()
        {
            try
            {
                _status = NonMonProcessingStatus.InProcess;

                if (OnStatusChanged != null)
                    OnStatusChanged(this, new EventArgs());

                int max = _records.Count;

                string host = ConfigurationManager.AppSettings.Get(Resources.Fields.CleoHost);
                int port = Convert.ToInt32(ConfigurationManager.AppSettings.Get(Resources.Fields.CleoPort));

                TPConnector connector = new TPConnector(host, port);
                try
                {
                    if (connector.Reserve(System.Configuration.ConfigurationManager.AppSettings.Get(Resources.Fields.TransactionSetName)) == 0)
                    {
                        INonMonRecord record;

                        while (_index < max)
                        {
                            record = _records[_index];

                            if ((_status == NonMonProcessingStatus.Aborted) ||
                                (_status == NonMonProcessingStatus.Paused))
                                break;

                            if (this.OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        (_index * 100) / max,
                                        String.Format("Processing {0} of {1}", _index + 1, max)));

                            PerformNonMon(record, connector);

                            if (this.OnRecordCompleted != null)
                                this.OnRecordCompleted(this, new NonMonRecordArgs(record));

                            _index++;
                        }

                        if (_index == max)
                        {
                            this._status = NonMonProcessingStatus.Completed;

                            if (OnStatusChanged != null)
                                OnStatusChanged(this, new EventArgs());

                            if (OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        100,
                                        String.Format(
                                            "{0} records processed", 
                                            max)));
                        }
                    }
                }
                finally
                {
                    connector.Release(true);
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private string TrimString(string inputString)
        {
            return (inputString == null) ? String.Empty : inputString.Trim();
        }
    }
}
