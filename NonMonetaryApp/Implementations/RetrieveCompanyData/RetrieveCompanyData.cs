using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.Configuration;
using TP;
using System.ComponentModel;
using System.Threading;
using log4net;
using NonMonetary.Implementations.RetrieveCompanyData.Resources;
using System.Reflection;
using System.Diagnostics;
using System.Xml.XPath;

namespace NonMonetary.Implementations.RetrieveCompanyData
{
    public class RetrieveCompanyData : INonMonProcessor
    {
        string _processorName;

        NonMonProcessingStatus _status = NonMonProcessingStatus.Pending;

        int _index = 0;

        List<INonMonRecord> _records;

        #region INonMonProcessor Members

        public event EventHandler<ProgressChangedEventArgs> OnProgressChanged;

        public event EventHandler<NonMonRecordArgs> OnRecordCompleted;

        public event EventHandler OnStatusChanged;

        public void Configure(IXPathNavigable xPathNavigable)
        {
        }

        public void ProcessNonMonRecords(List<INonMonRecord> records)
        {
            try
            {
                _index = 0;

                _records = records;

                Process();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private static void WriteLogEntry(
            LogEntryArgs logArgs
            )
        {
            Type declaringType = logArgs.MemberInfo.DeclaringType;

            log4net.ILog logger = log4net.LogManager.GetLogger(
                declaringType != null
                ? declaringType
                : logArgs.MemberInfo as Type
                );

            switch (logArgs.LogLevel)
            {
                case LogLevel.Fatal:
                    if (logger.IsFatalEnabled)
                        logger.Fatal(logArgs.ReadMessage());
                    break;
                case LogLevel.Error:
                    if (logger.IsErrorEnabled)
                        logger.Error(logArgs.ReadMessage());
                    break;
                case LogLevel.Warn:
                    if (logger.IsWarnEnabled)
                        logger.Warn(logArgs.ReadMessage());
                    break;
                case LogLevel.Info:
                    if (logger.IsInfoEnabled)
                        logger.Info(logArgs.ReadMessage());
                    break;
                case LogLevel.Debug:
                    if (logger.IsDebugEnabled)
                        logger.Debug(logArgs.ReadMessage());
                    break;
                default:
                    logger.Warn(LogMessages.FollowingEntryHasInvalidLevel);
                    logger.Info(logArgs.ReadMessage());
                    break;
            }
        }

        public void PerformNonMon(INonMonRecord item, TPConnector connector)
        {
            try
            {               

                item.Description = String.Format(
                    "Retrieving company data for id {0} ",
                    TrimString(item.GetFieldValue(Resources.Fields.CompanyID)));

                connector.ResetInput();
                connector.AddInput(Resources.Fields.SysPrinAgent, TrimString(item.GetFieldValue(Resources.Fields.SysPrinAgent)));
                connector.AddInput(Resources.Fields.CompanyID, TrimString(item.GetFieldValue(Resources.Fields.CompanyID)));

                item.ReturnCode = connector.RunTransaction(Resources.Fields.TRANS_RETRIEVE_COMPANY_DATA);
                
                if (item.ReturnCode == 0)
                {
                    StringBuilder sb = new StringBuilder();

                    item.ReturnMessage = TrimString(connector.GetOutput(Resources.Fields.ReturnMessage));
                    sb.Append("\"\",\"");                    
                    sb.Append(TrimString(item.GetFieldValue(Resources.Fields.NewSystem)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(item.GetFieldValue(Resources.Fields.NewPrin)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(item.GetFieldValue(Resources.Fields.NewAgent)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(item.GetFieldValue(Resources.Fields.NewSysPrinAgent)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(item.GetFieldValue(Resources.Fields.CompanyID)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.CompanyName)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.PrincipalOfficer)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.Address1)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.Address2)));
                    sb.Append("\",\"");
                    //sb.Append(TrimString(connector.GetOutput(Resources.Fields.Address3)));
                    //sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.City)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.State)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.Zipcode)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.FiscalYear)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.ContractExp)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.Misc1)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.Misc2)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.Misc3)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.Misc4)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.BillingAccountNo)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.BusType)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.FeeOption)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.CompanyContact)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.ContactTelephone)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.NextReviewDate)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.CreditScore)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.CreditLimit)));
                    sb.Append("\",\"");
                    //sb.Append(TrimString(connector.GetOutput(Resources.Fields.OpenDate)));
                    //sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.YearEstablished)));
                    sb.Append("\",\"");
                    sb.Append(TrimString(connector.GetOutput(Resources.Fields.CycleCode)));
                    sb.Append("\"");

                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\temp\CompanyData.txt", true))
                    {
                        file.WriteLine(sb.ToString());
                    } 

                }else
                    item.ReturnMessage = "Transaction Failed";
            
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        public string ProcessorName
        {
            get { return _processorName; }
            set { _processorName = value; }
        }

        public void Abort()
        {
            this._status = NonMonProcessingStatus.Aborted;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public void Continue()
        {
            if (_status == NonMonProcessingStatus.Paused)
            {
                Process();
            }
        }

        public void Pause()
        {
            this._status = NonMonProcessingStatus.Paused;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public NonMonProcessingStatus Status
        {
            get { return _status; }
        }

        #endregion

        private void Process()
        {
            try
            {
                _status = NonMonProcessingStatus.InProcess;

                if (OnStatusChanged != null)
                    OnStatusChanged(this, new EventArgs());

                int max = _records.Count;

                string host = ConfigurationManager.AppSettings.Get(Resources.Fields.CleoHost);
                int port = Convert.ToInt32(ConfigurationManager.AppSettings.Get(Resources.Fields.CleoPort));

                TPConnector connector = new TPConnector(host, port);
                try
                {
                    if (connector.Reserve(System.Configuration.ConfigurationManager.AppSettings.Get(Resources.Fields.TransactionSetName)) == 0)
                    {
                        INonMonRecord record;

                        while (_index < max)
                        {
                            record = _records[_index];

                            if ((_status == NonMonProcessingStatus.Aborted) ||
                                (_status == NonMonProcessingStatus.Paused))
                                break;

                            if (this.OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        (_index * 100) / max,
                                        String.Format("Processing {0} of {1}", _index + 1, max)));

                            PerformNonMon(record, connector);

                            if (this.OnRecordCompleted != null)
                                this.OnRecordCompleted(this, new NonMonRecordArgs(record));

                            _index++;
                        }

                        if (_index == max)
                        {
                            this._status = NonMonProcessingStatus.Completed;

                            if (OnStatusChanged != null)
                                OnStatusChanged(this, new EventArgs());

                            if (OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        100,
                                        String.Format(
                                            "{0} records processed", 
                                            max)));
                        }
                    }
                }
                finally
                {
                    connector.Release();
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private string TrimString(string inputString)
        {
            return (inputString == null) ? String.Empty : inputString.Trim();
        }
    }
}
