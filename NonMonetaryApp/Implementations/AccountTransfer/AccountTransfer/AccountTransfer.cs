using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NonMonetary.Interop;
using System.Diagnostics;
using System.Configuration;
using CardAssets.TP;
using System.ComponentModel;
using NonMonetary.Implementations.AccountTransfer.Resources;

namespace NonMonetary.Implementations.AccountTransfer
{
    public class AccountTransfer : INonMonProcessor
    {
        string _processorName;

        NonMonProcessingStatus _status = NonMonProcessingStatus.Pending;

        int _index = 0;

        List<INonMonRecord> _records;

        #region INonMonProcessor
        public void Abort()
        {
            this._status = NonMonProcessingStatus.Aborted;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public void Configure(System.Xml.XPath.IXPathNavigable xPathNavigable)
        {           
        }

        public void Continue()
        {
            if (_status == NonMonProcessingStatus.Paused)
            {
                Process();
            }
        }

        public event EventHandler<System.ComponentModel.ProgressChangedEventArgs> OnProgressChanged;

        public event EventHandler<NonMonRecordArgs> OnRecordCompleted;

        public event EventHandler OnStatusChanged;

        public void Pause()
        {
            this._status = NonMonProcessingStatus.Paused;

            if (OnStatusChanged != null)
                OnStatusChanged(this, new EventArgs());
        }

        public void ProcessNonMonRecords(List<INonMonRecord> records)
        {
            try
            {
                _index = 0;

                _records = records;

                Process();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        public string ProcessorName
        {
            get { return _processorName; }
            set { _processorName = value; }
        }

        public NonMonProcessingStatus Status
        {
            get { return _status; }
        }

        private static void WriteLogEntry(
           LogEntryArgs logArgs
           )
        {
            Type declaringType = logArgs.MemberInfo.DeclaringType;

            log4net.ILog logger = log4net.LogManager.GetLogger(
                declaringType != null
                ? declaringType
                : logArgs.MemberInfo as Type
                );

            switch (logArgs.LogLevel)
            {
                case LogLevel.Fatal:
                    if (logger.IsFatalEnabled)
                        logger.Fatal(logArgs.ReadMessage());
                    break;
                case LogLevel.Error:
                    if (logger.IsErrorEnabled)
                        logger.Error(logArgs.ReadMessage());
                    break;
                case LogLevel.Warn:
                    if (logger.IsWarnEnabled)
                        logger.Warn(logArgs.ReadMessage());
                    break;
                case LogLevel.Info:
                    if (logger.IsInfoEnabled)
                        logger.Info(logArgs.ReadMessage());
                    break;
                case LogLevel.Debug:
                    if (logger.IsDebugEnabled)
                        logger.Debug(logArgs.ReadMessage());
                    break;
                default:
                    logger.Warn(LogMessages.FollowingEntryHasInvalidLevel);
                    logger.Info(logArgs.ReadMessage());
                    break;
            }
        }
        #endregion INonMonProcessor


        private void Process()
        {
            try
            {
                _status = NonMonProcessingStatus.InProcess;

                if (OnStatusChanged != null)
                    OnStatusChanged(this, new EventArgs());

                int max = _records.Count;

                string host = ConfigurationManager.AppSettings.Get(Resources.Fields.CleoHost);
                int port = Convert.ToInt32(ConfigurationManager.AppSettings.Get(Resources.Fields.CleoPort));

                TPConnector connector = new TPConnector(host, port);
                try
                {
                    if (connector.Reserve(System.Configuration.ConfigurationManager.AppSettings.Get(Resources.Fields.TransactionSetName)) == 0)
                    {
                        INonMonRecord record;

                        while (_index < max)
                        {
                            record = _records[_index];

                            if ((_status == NonMonProcessingStatus.Aborted) ||
                                (_status == NonMonProcessingStatus.Paused))
                                break;

                            if (this.OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        (_index * 100) / max,
                                        String.Format("Processing {0} of {1}", _index + 1, max)));

                            PerformNonMon(record, connector);

                            if (this.OnRecordCompleted != null)
                                this.OnRecordCompleted(this, new NonMonRecordArgs(record));

                            _index++;
                        }

                        if (_index == max)
                        {
                            this._status = NonMonProcessingStatus.Completed;

                            if (OnStatusChanged != null)
                                OnStatusChanged(this, new EventArgs());

                            if (OnProgressChanged != null)
                                OnProgressChanged(
                                    this,
                                    new ProgressChangedEventArgs(
                                        100,
                                        String.Format(
                                            "{0} records processed",
                                            max)));
                        }
                    }
                }
                finally
                {
                    connector.Release(true);
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }

        private void PerformNonMon(INonMonRecord record, TPConnector connector)
        {
            try
            {
                if (!record.FieldValueExists(Resources.Fields.AccountNumber) || 
                    !record.FieldValueExists(Resources.Fields.LastName) ||                    
                    !record.FieldValueExists(Resources.Fields.NewSysPrinAgent))
                    throw new Exception(LogMessages.RecordMissingFields);

                record.Description = String.Format(
                    "Account transfer for account {0} to SysPrinAgent {1}.",
                    TrimString(record.GetFieldValue(Resources.Fields.AccountNumber)),
                    TrimString(record.GetFieldValue(Resources.Fields.NewSysPrinAgent)));

                connector.resetInput();

                connector.addInput(Resources.Fields.AccountNumber, TrimString(record.GetFieldValue(Resources.Fields.AccountNumber)));
                connector.addInput(Resources.Fields.TransferType, "N");
                connector.addInput(Resources.Fields.LastName, TrimString(record.GetFieldValue(Resources.Fields.LastName)));
                connector.addInput(Resources.Fields.LetterNumber, TrimString(record.GetFieldValue(Resources.Fields.LetterNumber)));
                connector.addInput(Resources.Fields.ProducePlastics, "Y");
                
                if (String.IsNullOrEmpty(record.GetFieldValue(Resources.Fields.NewAccountNumber)))
                {
                    connector.addInput(Resources.Fields.NewSysPrinAgent, TrimString(record.GetFieldValue(Resources.Fields.NewSysPrinAgent)));
                    connector.addInput(Resources.Fields.AutoAccountAssign, "Y");
                    connector.addInput(Resources.Fields.NewAccountNumber, String.Empty);
                }
                else
                {
                    connector.addInput(Resources.Fields.NewSysPrinAgent, String.Empty);
                    connector.addInput(Resources.Fields.AutoAccountAssign, String.Empty);
                    connector.addInput(Resources.Fields.NewAccountNumber, TrimString(record.GetFieldValue(Resources.Fields.NewAccountNumber)));
                }

                if (String.IsNullOrEmpty(TrimString(record.GetFieldValue(Resources.Fields.RushIndicator))))
                    connector.addInput(Resources.Fields.RushIndicator, String.Empty);
                else
                    connector.addInput(Resources.Fields.RushIndicator, TrimString(record.GetFieldValue(Resources.Fields.RushIndicator)));

                record.ReturnCode = connector.runTransaction(Resources.Fields.AccountTransfer);

                if (record.ReturnCode == 0)
                    record.ReturnMessage = TrimString(connector.getOutput(Resources.Fields.ReturnMessage));
                else
                    record.ReturnMessage = "Transaction Failed";

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace(1);
                WriteLogEntry(new LogEntryArgs(
                    LogLevel.Fatal,
                    stackTrace.GetFrame(0).GetMethod(),
                    ex.Message));
            }
        }
        private string TrimString(string inputString)
        {
            return (inputString == null) ? String.Empty : inputString.Trim();
        }
    }
}
